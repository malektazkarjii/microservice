import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import dotenv from "dotenv";
import country from "./Routes/CountryRoutes.js";

const app = express();
app.use(cors());
dotenv.config();


app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const PORT = process.env.PORT || 6600;

mongoose.connect(process.env.MONGO_URL, (err, connect) => {
  if (err) console.log(`Error: ${err.message}`);
  else {
    console.log(`MongoDB connected:${connect.host}`);
    app.listen(PORT, () => {
      console.log(`Server is listening on port ${PORT}`);
    });
  }
});

app.use("/api/country", country);


app.use("*", (req, res) => {
  res.status(404).json({
    status: 404,
    message: "Page Not Found! Please enter a valid URL to proceed",
  });
});
