import express from "express";
import COUNTRY from "../Controllers/CountryinfoControllers.js";

const router = express.Router();

router.get("/check/:number", COUNTRY.Phonenumbervalidation);
router.get("/:number", COUNTRY.getNumberInfo);
router.get("/", COUNTRY.getAllPhoneInfo);
router.post("/create", COUNTRY.AddNumberInfo);
router.delete("/delete", COUNTRY.deleteNumberInfo);

export default router;